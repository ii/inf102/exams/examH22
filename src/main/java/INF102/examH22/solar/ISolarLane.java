package INF102.examH22.solar;

import java.util.List;

/**
 * This class is a simplified version of semesteroppgave1 Fall 2022.
 * All solar panels are placed on a line, all jobs require only 1 robot 
 * and jobs come so slow that we can assume only 1 job is active at a time.
 * 
 * @author Martin Vatshelle
 */
public interface ISolarLane {

	/**
	 * This method computes the optimal placement of a robot.
	 * Given a list of jobs (past locations for jobs) we want to minimize
	 * the expected time for next job to be done.
	 * This is done by selection the index in the list of jobs
	 * such that sum over all i of Math.abs(jobs.get(i) - jobs.get(index)) is minimized.
	 * @param jobs - the list of past locations
	 * @return an optimal location to place the robot
	 */
	public Integer place1Robot(List<Integer> jobs);
	
	/**
	 * This method computes the optimal placement of two robots.
	 * Given a list of jobs (past locations for jobs) we want to minimize
	 * the expected time for next job to be done.
	 * This is done by selection two indices index1 and index2 
	 * in the list of jobs such that: 
	 * sum over all i of dist(i) is minimized.
	 * Where dist(i) is defined as 
	 * min of Math.abs(jobs.get(i) - jobs.get(index1)) and Math.abs(jobs.get(i) - jobs.get(index2))
	 * @param jobs - the list of past locations
	 * @return an optimal location to place the robot
	 */
	public Pair<Integer> place2Robots(List<Integer> jobs);

}

/**
 * This class stores a pair
 * @author Martin Vatshelle
 */
class Pair<V>{
	V loc1;
	V loc2;
	public Pair(V loc1, V loc2) {
		this.loc1 = loc1;
		this.loc2 = loc2;
	}	
}
